package com.bbs.customerlist.Enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum InsuranceGrade {
    RAITING1("1등급"),
    RAITING2("2등급"),
    RAITING3("3등급"),
    RAITING4("4등급");

    private final String name;

}
