package com.bbs.customerlist.model;

import com.bbs.customerlist.Enums.Gender;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerListStatusChangeRequest {
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

}
