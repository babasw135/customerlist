package com.bbs.customerlist.model;

import com.bbs.customerlist.Enums.Gender;
import com.bbs.customerlist.Enums.InsuranceGrade;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CustomerListitem {
    private Long id;
    private String name;
    private LocalDate requestDate;
    private LocalDate reservationDate;
    private String birthDate;
    private String phoneNumber;
    private String address;
    private String gender;
    private String insuranceGrade;
    private String memo;
}
