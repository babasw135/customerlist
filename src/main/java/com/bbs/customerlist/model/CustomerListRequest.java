package com.bbs.customerlist.model;

import com.bbs.customerlist.Enums.Gender;
import com.bbs.customerlist.Enums.InsuranceGrade;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CustomerListRequest {
    private String name;
    private LocalDate reservationDate;
    private String birthDate;
    private String phoneNumber;
    private String address;

    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @Enumerated(value = EnumType.STRING)
    private InsuranceGrade insuranceGrade;

    private String memo;
}
