package com.bbs.customerlist.repository;

import com.bbs.customerlist.Entity.CustomerList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerListRepository extends JpaRepository<CustomerList, Long> {
}
