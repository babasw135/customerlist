package com.bbs.customerlist.service;


import com.bbs.customerlist.Entity.CustomerList;
import com.bbs.customerlist.model.CustomerListRequest;
import com.bbs.customerlist.model.CustomerListResponse;
import com.bbs.customerlist.model.CustomerListStatusChangeRequest;
import com.bbs.customerlist.model.CustomerListitem;
import com.bbs.customerlist.repository.CustomerListRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerListService {
    private final CustomerListRepository customerListRepository;

    public void setCustomerList(CustomerListRequest request){
        CustomerList addData = new CustomerList();
        addData.setName(request.getName());
        addData.setRequestDate(LocalDate.now());
        addData.setReservationDate(request.getReservationDate());
        addData.setBirthDate(request.getBirthDate());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setAddress(request.getAddress());
        addData.setGender(request.getGender());
        addData.setInsuranceGrade(request.getInsuranceGrade());
        addData.setMemo(request.getMemo());

        customerListRepository.save(addData);
    }

    public List<CustomerListitem> getitem(){
    List<CustomerList> originList = customerListRepository.findAll();

    List<CustomerListitem> result = new LinkedList<>();

    for (CustomerList customerList : originList) {
    CustomerListitem additem = new CustomerListitem();
    additem.setId(customerList.getId());
    additem.setName(customerList.getName());
    additem.setRequestDate(customerList.getRequestDate());
    additem.setReservationDate(customerList.getReservationDate());
    additem.setBirthDate(customerList.getBirthDate());
    additem.setPhoneNumber(customerList.getPhoneNumber());
    additem.setAddress(customerList.getAddress());
    additem.setGender(customerList.getGender().getName());
    additem.setInsuranceGrade(customerList.getInsuranceGrade().getName());
    additem.setMemo(customerList.getMemo());

    result.add(additem);
    }
    return result;
    }
    public CustomerListResponse getCustomerList(long id){
        CustomerList originData = customerListRepository.findById(id).orElseThrow();

        CustomerListResponse response = new CustomerListResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setRequestDate(originData.getRequestDate());
        response.setReservationDate(originData.getReservationDate());
        response.setBirthDate(originData.getBirthDate());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setAddress(originData.getAddress());
        response.setGender(originData.getGender().getName());
        response.setInsuranceGrade(originData.getInsuranceGrade().getName());
        response.setMemo(originData.getMemo());

        return response;
    }

    public void putCustomerList(long id, CustomerListStatusChangeRequest request){
        CustomerList originDate = customerListRepository.findById(id).orElseThrow();
        originDate.setGender(request.getGender());

        customerListRepository.save(originDate);
    }

}

