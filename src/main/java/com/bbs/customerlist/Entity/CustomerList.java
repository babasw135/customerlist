package com.bbs.customerlist.Entity;


import com.bbs.customerlist.Enums.Gender;
import com.bbs.customerlist.Enums.InsuranceGrade;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class CustomerList {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private LocalDate requestDate;
    @Column(nullable = false)
    private LocalDate reservationDate;
    @Column(nullable = false)
    private String birthDate;
    @Column(nullable = false)
    private String phoneNumber;
    @Column(nullable = false)
    private String address;
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private InsuranceGrade insuranceGrade;
    @Column(columnDefinition = "TEXT")
    private String memo;
}

