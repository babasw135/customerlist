package com.bbs.customerlist.controller;

import com.bbs.customerlist.model.CustomerListRequest;
import com.bbs.customerlist.model.CustomerListResponse;
import com.bbs.customerlist.model.CustomerListStatusChangeRequest;
import com.bbs.customerlist.model.CustomerListitem;
import com.bbs.customerlist.service.CustomerListService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer-list")
public class CustomerListController {
    public final CustomerListService customerListService;

    @PostMapping("/new")
    public String setCustomerList(@RequestBody CustomerListRequest request){
        customerListService.setCustomerList(request);

        return  "ok";
    }
    @GetMapping("/aoo")
    public List<CustomerListitem> getitem(){
        return customerListService.getitem();
    }
    @GetMapping("/detail/{id}")
    public CustomerListResponse getCustomerList(@PathVariable long id){
        return customerListService.getCustomerList(id);
    }
    @PutMapping("/change/{id}")
    public String putCustomerList(@PathVariable long id, @RequestBody CustomerListStatusChangeRequest request){
        customerListService.putCustomerList(id, request);

        return "ok";
    }
}
